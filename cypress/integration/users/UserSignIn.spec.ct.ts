describe('[User] Sign into Anidopt', () => {
  it('Renders the page', () => {
    cy.visit('/sign-in', {
      headers: {
        'Accept-Encoding': 'gzip, deflate'
      }
    })
    cy.get('.ani-content').get('main.sign-in-page')
  })

  it('Inputs the data', () => {
    const dummyShelterMail = Cypress.env('VALID_SHELTER_TEST_MAIL')
    const dummyShelterPassword = Cypress.env('VALID_SHELTER_TEST_PASSWORD')

    cy.get('.ani-form').within(() => {
      cy.get('#email').type(dummyShelterMail)
      cy.get('#password').type(dummyShelterPassword)
    })

    cy.get('button[type=submit]').click()
  })

  it('Logs the user in', () => {
    cy.getCookie('anidoptuser')
  })
})

describe('[User] Tries to sign in with the wrong password', () => {
  it('Renders the page', () => {
    cy.visit('/sign-in', {
      headers: {
        'Accept-Encoding': 'gzip, deflate'
      }
    })
    cy.get('.ani-content').get('main.sign-in-page')
  })

  it('Inputs the data', () => {
    const dummyShelterMail = Cypress.env('VALID_SHELTER_TEST_MAIL')
    const dummyShelterPassword = '123'

    cy.get('.ani-form').within(() => {
      cy.get('#email').type(dummyShelterMail)
      cy.get('#password').type(dummyShelterPassword)
    })

    cy.get('button[type=submit]').click()
  })

  it('Sends an explicit error message', () => {
    cy.get('.form-error-handler').contains("L'authentification a échouée ou votre compte n'est pas vérifié ; veuillez vérifiez vos identifiants ou votre boîte mail.")
  })
})
