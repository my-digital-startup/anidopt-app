describe('[User] Sign out from Anidopt', () => {
  it('Renders the logging component', () => {
    cy.visit('/sign-in')
  })

  it('Inputs the data to log in', () => {
    const dummyShelterMail = Cypress.env('VALID_SHELTER_TEST_MAIL')
    const dummyShelterPassword = Cypress.env('VALID_SHELTER_TEST_PASSWORD')

    cy.get('.ani-form').within(() => {
      cy.get('#email').type(dummyShelterMail)
      cy.get('#password').type(dummyShelterPassword)
    })

    cy.get('button[type=submit]').click()
  })

  it('Logs the user in', () => {
    cy.getCookie('anidoptuser')
  })

  it('Logs the user out', () => {
    cy.get('#logout').click()

    cy.get('#login')
  })
})
