export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Anidopt',
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      // Meta vanilla
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
      { hid: 'description', name: 'description', content: 'Des dizaines de refuges animaliers basés sur Rennes vous ouvrent leurs portes - Adoptez votre nouveau compagnon, ou parrainez les refuges rennais proches de votre coeur !' },

      // Favicon
      { name: 'msapplication-TileColor', color: '#da532c' },
      { name: 'theme-color', color: '#ffffff' }
    ],
    link: [
      // Favicon
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicon/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon/favicon-32x32.ico' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon/favicon-16x16.ico' },
      { rel: 'manifest', href: '/favicon/site.webmanifest' },
      { rel: 'mask-icon', color: '#5bbad5', href: '/favicon/safari-pinned-tab.svg' },

      // Stylesheets
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&family=Nunito:ital,wght@0,700;0,900;1,700;1,900&display=swap'
      }
    ],
    script: [
      { }
    ]
  },
  target: 'server',
  ssr: true,

  css: [
    '~/assets/scss/_global.scss'
  ],

  styleResources: {
    scss: [
      '~/assets/scss/_mixins.scss',
      '~/assets/scss/_variables.scss'
    ]
  },

  plugins: [
    '~/plugins/anidopt-api'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    '~/components/',
    { path: '~/components/atoms/', prefix: 'ato' },
    { path: '~/components/organisms/', prefix: 'ani' }
  ],

  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/fontawesome'
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/style-resources',
    '@nuxtjs/auth-next',
    '@nuxt/image'
  ],

  compilerOptions: {
    types: [
      '@nuxt/types',
      '@nuxtjs/axios',
      '@nuxtjs/auth-next',
      '@nuxt/image'
    ]
  },

  render: {
    ssrLog: true
  },

  auth: {
    // Redirections : Users are only redirected to home if the routes are protected
    rewriteRedirects: false,
    watchLoggedin: false,
    redirect: {
      login: '/sign-in',
      logout: '/',
      callback: false,
      home: '/'
    },

    // Define cookie global values
    localStorage: false,
    cookie: {
      prefix: 'anidopt',
      options: {
        path: '/'
      }
    },

    // Strategy logic
    strategies: {
      local: {
        token: {
          property: 'token',
          required: true,
          global: true,
          type: 'Bearer'
        },

        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },

        endpoints: {
          login: { url: '/api/v1/login_check', method: 'post' },
          user: { url: '/api/v1/users/me', method: 'get', property: false },
          refresh: { url: '/api/v1/token/refresh', method: 'get', property: false },
          logout: false
        },

        user: {
          property: false
        }
      }
    }
  },

  axios: {
    proxy: true
  },

  proxy: {
    '/api/': {
      target: 'https://anidopt-api.juliendrieu.fr',
      secure: false
    }
  },

  fontawesome: {
    icons: {
      solid: [
        'faEye',
        'faEyeSlash',
        'faLink',
        'faUnlink',
        'faBold',
        'faItalic',
        'faStrikethrough',
        'faListUl',
        'faUndoAlt',
        'faRedoAlt',
        'faCog',
        'faSignOutAlt',
        'faEdit',
        'faPlusSquare',
        'faEraser',
        'faCircleNotch',

        'faHome',
        'faPaw',
        'faHeart',
        'faSeedling',
        'faUser',
        'faLightbulb',
        'faHandshake',
        'faThLarge',
        'faPaperPlane',
        'faClock',
        'faQuestionCircle',
        'faTag',

        'faMars',
        'faVenus',
        'faMapMarker',
        'faCalendar',
        'faAngleDoubleUp',
        'faAngleDoubleDown',
        'faCheck',
        'faTimes',
        'faBars'
      ],
      regular: [
        'faHeart'
      ]
    }
  },

  image: {
    screens: {
      sm: 800,
      md: 1200
    },

    presets: {
      cover: {
        modifiers: {
          fit: 'cover',
          format: 'webp'
        }
      }
    }
  },

  env: {
    API_BASE_URL: process.env.API_BASE_URL
  }
}
