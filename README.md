# Anidopt App

Anidopt, an application centered on giving abandonned friends a new home.

Proudly constructed and built with NuxtJS.

# Installation
## Locally
```bash
# Install dependencies
npm install

# Run the dev build for testing purposes
npm run dev

# Generate files and run for production
npm run build
npm run start
```

# Changelog
## 0.5.1
This version adds the option to update the cover photo of the card of the animal.

## 0.5.0
This version adds the new pet management system for shelters. Shelter accounts can now :
* View their pets in a slider view (Will probably be refactored with user tests)
* Add new pets
* Modify their existing pets
* Delete their pets (Will probably need to be archived later)

All these features are linked to the store and updates it when they need to for consistency. The API module was updated following all these features.

## 0.4.3
This version implements a bunch of pages and fixes a MAJOR drawback of how models worked, and fully enables SSR without conflict between client / server.
## 0.4.2
This version fixes a bunch of routes, sets the ground for more user actions, and some loading issues.
It also adds responsive design to the main view.
## 0.4.1
This version fixes a visual bug on the WYSIWYG editor (broken icons)
## 0.4.0
This version implements a basic getAllPets implementation. It will be refactored in the future when the design evolves, but the functionnality is there, kinda.
## 0.3.2
This version fixes a bunch of problems with how error messages were handled.
## 0.3.1
This version reworks a bit of its UI. Nothing "functionnal" form the API has been added, so this is safe to use with previous versions.
## 0.3.0
**This version has been tested with the version 0.7.7 of the API**

This version adds the possibility for a guest to reset their password if they ended up forgetting it. **The logic is broken down into two states** : one form submits a request and the second confirms the reset.

The application swallows one API error to avoid malicious users to gather data on accounts, and the email system prevents anyone from having access to another person's data.

It also includes a brand new Atom component (the very first of this project to be honest) to handle passwords and sensitive data. The AtoPasswordInput can, given a string, toggle its visibility and is entirely usable with the keyboard.

## 0.2.2
This version just adds a valid error handler for the LoginForm component. The errors will be pulled from the API, but the app has a built-in default message if the API doesn't send one back.

Some console.log statements have been removed and cleaned up.

## 0.2.1
This version expands on the login system : it now sets cookies, deletes them, and uses the middleware of @nuxtjs/auth-next to prevent users from accessing spaces that they aren't allowed in (for instance, a logged in user having access to the registration page, or a guest using a non-existent dashboard)

## 0.2.0
This version implements a basic login system and the same barebones design.

For now, the Login form returns a JWT token in the console, the next versions will fully implement the login system of the API, to, for instance, get the current user.

This will have to wait for the API implementation.

### **Next steps**
- Add the axios config to get the current user via the JWT
- Implement a rudimentary store for at least Users
- Implement cookies to keep the session active
- Implement the logout function

## 0.1.1
This version overhauls the registration process and fixes some bugs. Overall, the previous "Next steps" have been acknowledged and implemented.
## 0.1.0
**Compatible only with the API version 0.2.0 and up !**

This version implements a basic registration process and a very barebones design ; this was done to maximize the dev output while the wireframes and design layouts are still being created.

The registration form handles both types of User accounts, features a bunch of data parsing and protection against malicious inputs.

### **Next steps**
- Add a confirmation page 
- Add a loader while the API is processing data
- Redirecting the user without confusing them
- Add warnings if the email / name is already in use
