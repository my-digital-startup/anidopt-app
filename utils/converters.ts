import { accountType } from './types'
import { Shelter } from '~/models/users/Shelter'
import { Adopter } from '~/models/users/Adopter'
import { User } from '~/models/users/User'

export function formatIsoString (isoString: string): string {
  const date = new Date(isoString)
  return date.toLocaleString('default', { day: 'numeric', month: 'long', year: 'numeric', hour: 'numeric', minute: 'numeric', timeZone: 'UTC' })
}

export function formatUserInitials (user: User | Shelter | Adopter) {
  if (accountType(user) === 'shelter') {
    const shelter = user as Shelter
    return shelter.slug.substring(0, 2).toUpperCase()
  } else {
    const adopter = user as Adopter
    const firstName: string | null = adopter.firstName
    const lastName: string | null = adopter.lastName

    let initials: string

    initials = (typeof firstName === 'string') ? firstName.charAt(0) : ''
    initials += (typeof lastName === 'string') ? lastName.charAt(0) : ''

    return initials.toUpperCase() || 'ANI'
  }
}
