/**
 * Resets the current error data that the form holds
 */
export const resetFormErrors = (errors: any): any => {
  let k: keyof typeof errors
  for (k in errors) {
    errors[k] = ''
  }
  return errors
}

/**
 * Check if the form contains any error, to prevent itself from sending anything to the API
 */
export const isFormValid = (errors: any): boolean => {
  let hasErrors = true

  let k: keyof typeof errors
  for (k in errors) {
    if (errors[k] !== '') {
      hasErrors = false
    }
  }

  return hasErrors
}

/**
 * Check if a mail is following a correct and acceptable syntax
 *
 * @param mail The mail to test
 */
export const isMailValid = (mail: string): boolean => {
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return regex.test(String(mail).toLowerCase())
}

/**
 * Check if a string contains an XSS injection attempt
 *
 * @param data The string to test
 */
export const containsScriptTag = (data: string): boolean => {
  const regex = /<script[\s\S]*?>[\s\S]*?<\/script>/gi
  return regex.test(String(data))
}
