/**
 * Check the type of account of any object
 */
export const accountType = (user: any): 'adopter' | 'shelter' | 'unknown' => {
  let userRole: 'adopter' | 'shelter' | 'unknown' = 'unknown'

  user.roles.forEach((role: string) => {
    if (role === 'ROLE_ADOPTER') {
      userRole = 'adopter'
    } else if (role === 'ROLE_SHELTER') {
      userRole = 'shelter'
    }
  })

  return userRole
}
