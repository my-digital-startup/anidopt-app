/* eslint-disable import/no-mutable-exports */
import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'

import GlobalStore from '~/store/globals'
import PetStore from '~/store/pets'

let petStore: PetStore
let globalStore: GlobalStore

function initialiseStores (store: Store<any>): void {
  globalStore = getModule(GlobalStore, store)
  petStore = getModule(PetStore, store)
}

export { initialiseStores, globalStore, petStore }
