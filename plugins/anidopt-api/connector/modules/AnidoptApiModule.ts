import { Context } from '@nuxt/types'
import { apiVersion } from '../..'

/**
 * Base module to extend from if you want to build modules that interact with the API
 *
 * @property resource - API path
 * @property instance - Axiosinstance
 */
export class AnidoptApiModule {
  private readonly _apiVersion = apiVersion
  private _resource: string
  private _context: Context

  constructor (resource: string, context: Context) {
    this._resource = `/api/${this._apiVersion}${resource}`
    this._context = context
  }

  get resource (): string {
    return this._resource
  }

  get context (): Context {
    return this._context
  }
}
