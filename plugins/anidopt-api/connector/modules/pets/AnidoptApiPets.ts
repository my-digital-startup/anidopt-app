import { Context } from '@nuxt/types'

import { AnidoptApiModule } from '../AnidoptApiModule'
import { AnidoptApiBreeds } from './AnidoptApiBreeds'
import { AnidoptApiSpecies } from './AnidoptApiSpecies'
import { AnidoptApiTags } from './AnidoptApiTags'

import { Pet, PetFactory } from '~/models/pets/Pet'
import { apiVersion } from '~/plugins/anidopt-api'

export class AnidoptApiPets extends AnidoptApiModule {
  private _breeds: AnidoptApiBreeds
  private _species: AnidoptApiSpecies
  private _tags: AnidoptApiTags

  constructor (context: Context) {
    super('/pets', context)

    this._breeds = new AnidoptApiBreeds(this.context)
    this._species = new AnidoptApiSpecies(this.context)
    this._tags = new AnidoptApiTags(this.context)
  }

  get breeds (): AnidoptApiBreeds {
    return this._breeds
  }

  get species (): AnidoptApiSpecies {
    return this._species
  }

  get tags (): AnidoptApiTags {
    return this._tags
  }

  public getAll = async (paginated = false, _pageNum = 1): Promise<Pet[]> => {
    const res = await this.context.$axios.get(this.resource, { params: { pagination: paginated } })
    const data = res.data['hydra:member'] as Array<any>
    const pets = [] as Pet[]

    data.forEach((pet) => {
      pets.push(PetFactory.fromJson(pet))
    })

    return pets
  }

  public getOneByUuid = async (uuid: string): Promise<Pet> => {
    const res = await this.context.$axios.get(`${this.resource}/${uuid}`)

    return PetFactory.fromJson(res.data)
  }

  public addOne = async (jsonPet: any, shelterUuid: string): Promise<Pet> => {
    jsonPet.specie = `/api/${apiVersion}/species/${jsonPet.species.id}`
    jsonPet.breeds = jsonPet.breeds.map((breed: any) => `/api/${apiVersion}/breeds/${breed.id}`)
    jsonPet.tags = jsonPet.tags.map((tag: any) => `/api/${apiVersion}/tags/${tag.id}`)
    jsonPet.shelter = `/api/${apiVersion}/shelters/${shelterUuid}`

    delete jsonPet.species
    delete jsonPet.images
    delete jsonPet.mainImage

    const res = await this.context.$axios.post(`${this.resource}`, jsonPet)

    return PetFactory.fromJson(res.data)
  }

  public updateOne = async (petUuid: string, pet: Pet): Promise<Pet> => {
    const jsonPet = { ...pet } as any

    jsonPet.specie = `/api/${apiVersion}/species/${pet.species.id}`
    jsonPet.breeds = jsonPet.breeds.map((breed: any) => `/api/${apiVersion}/breeds/${breed.id}`)
    jsonPet.tags = jsonPet.tags.map((tag: any) => `/api/${apiVersion}/tags/${tag.id}`)
    jsonPet.mainImage = jsonPet.mainImage['@id']

    delete jsonPet.species
    delete jsonPet.images
    delete jsonPet.shelter

    const res = await this.context.$axios.patch(`${this.resource}/${petUuid}`, jsonPet, { headers: { 'Content-Type': 'application/merge-patch+json' } })

    return PetFactory.fromJson(res.data)
  }

  public deletePet = (uuid: string) => {
    return this.context.$axios.delete(`${this.resource}/${uuid}`)
  }
}
