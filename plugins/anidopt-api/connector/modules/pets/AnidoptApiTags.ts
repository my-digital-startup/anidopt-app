import { Context } from '@nuxt/types'
import { AnidoptApiModule } from '../AnidoptApiModule'
import { Category, CategoryFactory } from '~/models/pets/Category'

export class AnidoptApiTags extends AnidoptApiModule {
  constructor (context: Context) {
    super('/tags', context)
  }

  public getAll = async (): Promise<Category[]> => {
    const res = await this.context.$axios.get(this.resource)
    const data = res.data['hydra:member'] as Array<any>
    const tags = [] as Category[]

    data.forEach((breed) => {
      tags.push(CategoryFactory.fromJson(breed))
    })

    return tags
  }
}
