import { Context } from '@nuxt/types'
import { AnidoptApiModule } from '../AnidoptApiModule'
import { Category, CategoryFactory } from '~/models/pets/Category'

export class AnidoptApiBreeds extends AnidoptApiModule {
  constructor (context: Context) {
    super('/breeds', context)
  }

  public getAll = async (): Promise<Category[]> => {
    const res = await this.context.$axios.get(this.resource)
    const data = res.data['hydra:member'] as Array<any>
    const breeds = [] as Category[]

    data.forEach((breed) => {
      breeds.push(CategoryFactory.fromJson(breed))
    })

    return breeds
  }
}
