import { Context } from '@nuxt/types'
import { AnidoptApiModule } from '../AnidoptApiModule'
import { Category, CategoryFactory } from '~/models/pets/Category'

export class AnidoptApiSpecies extends AnidoptApiModule {
  constructor (context: Context) {
    super('/species', context)
  }

  public getAll = async (): Promise<Category[]> => {
    const res = await this.context.$axios.get(this.resource)
    const data = res.data['hydra:member'] as Array<any>
    const species = [] as Category[]

    data.forEach((specie) => {
      species.push(CategoryFactory.fromJson(specie))
    })

    return species
  }
}
