import { Context } from '@nuxt/types'
import { HTTPResponse } from '@nuxtjs/auth-next'
import { AnidoptApiModule } from '../AnidoptApiModule'

import { AnidoptApiShelters } from './AnidoptApiShelters'
import { AnidoptApiAdopters } from './AnidoptApiAdopters'
import { User } from '~/models/users/User'

/**
 * API Module that handles User models and their functions
 *
 * The class has sub-modules to handle specific User entities
 *
 * @function registerOne()
 * @function login()
 */
export class AnidoptApiUsers extends AnidoptApiModule {
  private _adopters: AnidoptApiAdopters
  private _shelters: AnidoptApiShelters

  constructor (context: Context) {
    super('/users', context)

    this._adopters = new AnidoptApiAdopters(context)
    this._shelters = new AnidoptApiShelters(context)
  }

  get adopters (): AnidoptApiAdopters {
    return this._adopters
  }

  get shelters (): AnidoptApiShelters {
    return this._shelters
  }

  /**
   * Registers an user account and creates the model in the database
   * The API is supposed to send an email to allow the user to use their new account
   *
   * This function is separated in two different implementations :
   * @function registerShelter()
   * @function registerAdopter()
   *
   * @param email The unique email of the new account
   * @param password The password of the account to be hashed and stored
   * @param name (required for shelters) The name that will be used as a slug for the shelter
   * @param isShelter (optional) If set, treats the new account as a shelter instance
   *
   * [More information on the needed data](https://gitlab.com/my-digital-startup/anidopt-api-documentation)
   */
  public registerOne (data: { email: string, password: string, name: string, isShelter?: boolean }): Promise<User> {
    if (data.name && data.isShelter) {
      return this.shelters.registerOne({ email: data.email, password: data.password, name: data.name })
    } else {
      return this.adopters.registerOne({ pseudo: data.name, email: data.email, password: data.password })
    }
  }

  /**
   * Logs a user in through the $auth module that's injected into the context
   *
   * The relevant user data can be then fetched via $auth.$storage.getState('user')
   *
   * @param name The email adresse of the given account
   * @param password The plaintext password of the account
   */
  public async login (user: { name: string, password: string }): Promise<void | User> {
    await this.context.$auth.loginWith('local', { data: { username: user.name, password: user.password } })

    const currentUser = this.context.$auth.user as any
    const roles = currentUser?.roles as Array<string>
    const isShelter = roles.includes('ROLE_SHELTER')

    if (isShelter) {
      this.context.$auth.$storage.setUniversal('user', currentUser)
    } else {
      this.context.$auth.$storage.setUniversal('user', currentUser)
    }
  }

  /**
   * Logs a user out through the $auth module that's injected into the context
   */
  public logout (): Promise<void> {
    this.context.$auth.$storage.removeUniversal('user')
    return this.context.$auth.logout()
  }

  /**
   * Either request or confirm a password reset on a given account
   *
   * @param email The email that identifies the account
   * @param token (optional) The password reset token (32 characters)
   * @param password (optional) The new password that will replace the old one
   */
  public forgotPassword (data: { email: string, token?: string, password?: string }): Promise<HTTPResponse> {
    return this.context.$axios.put(`${this.resource}/password/reset`, data)
      .catch((err) => {
        throw err
      })
  }

  /**
   * Given a user UUID, changes their password. Usually, the user should be logged in to access this resource.
   *
   * @param uuid The user UUID, more often than not is the logged in user in this.$auth
   * @param oldPassword The old value of the user's password
   * @param newPassword The new value to be set as password
   */
  public changePassword (uuid: string, data: { oldPassword: string, newPassword: string }): Promise<HTTPResponse> {
    return this.context.$axios.patch(`${this.resource}/${uuid}/password`, data, { headers: { 'Content-Type': 'application/merge-patch+json' } })
      .catch((err) => {
        throw err
      })
  }
}
