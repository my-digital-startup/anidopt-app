import { Context } from '@nuxt/types'
import { AnidoptApiModule } from '../AnidoptApiModule'

import { Shelter, ShelterFactory } from '~/models/users/Shelter'
import { Pet } from '~/models/pets/Pet'

/**
 * API Module that handles Adopter models and their functions
 *
 * @function getAll()
 * @function getOneByUUID()
 * @function registerOne()
 */
export class AnidoptApiShelters extends AnidoptApiModule {
  constructor (context: Context) {
    super('/shelters', context)
  }

  /**
   * Get all Shelters from the API
   *
   * @returns An AxiosResponse with a Shelter[] on success
   */
  public async getAll (name?: string): Promise<Shelter[]> {
    try {
      let query = this.resource + '?'

      if (name) {
        query += `name=${name.split('-')[0]}`
      }

      const res = await this.context.$axios.get(query)

      const data = res.data['hydra:member'] as Array<any>
      const output = [] as Shelter[]

      data.forEach((user) => {
        output.push(ShelterFactory.fromJson(user))
      })

      return output
    } catch (err) {
      console.log(err)
      throw err
    }
  }

  /**
   * Get a single Shelter from the API
   *
   * @param id A unique integer
   * @returns An AxiosResponse with a single Shelter instance on success
   */
  public getOneByUUID (uuid: string): Promise<Shelter> {
    return this.context.$axios.get(`${this.resource}/${uuid}`)
      .then(res => ShelterFactory.fromJson(res.data))
      .catch((err) => {
        throw err
      })
  }

  /**
   * Creates a Shelter account specifically
   *
   * [More information on the needed data](https://gitlab.com/my-digital-startup/anidopt-api-documentation)
   *
   * @param data An object that holds the specific data to create a Shelter account
   * @returns A promise that resolves a Shelter object or an error
   */
  public registerOne (data: { email: string, password: string, name: string }): Promise<Shelter> {
    return this.context.$axios.post(this.resource, data)
      .then(res => ShelterFactory.fromJson(res.data))
      .catch((err) => {
        console.log(err)
        throw err
      })
  }

  public addPetFromShelter = async (pet: any, shelterUuid: string) => {
    let newPet: Pet

    try {
      newPet = await this.context.$api.pets.addOne(pet, shelterUuid)
    } catch (err) {
      console.log(err)
      throw err
    }

    await this.context.$auth.fetchUser()
    return newPet
  }

  public updatePetFromShelter = async (petUuid: string, pet: Pet): Promise<Pet> => {
    let updatedPet: Pet

    try {
      updatedPet = await this.context.$api.pets.updateOne(petUuid, pet)
    } catch (err) {
      console.log(err)
      throw err
    }

    await this.context.$auth.fetchUser()
    return updatedPet
  }

  public deletePetFromShelter = async (petUuid: string) => {
    try {
      await this.context.$api.pets.deletePet(petUuid)
    } catch (err) {
      console.log(err)
      throw err
    }

    const updatedUser = { ...this.context.$auth.user }

    let updatedPets = updatedUser.pets as Array<Pet>

    updatedPets = updatedPets.filter(pet => pet.uuid !== petUuid)
    updatedUser.pets = updatedPets
    this.context.$auth.setUser(updatedUser)
  }
}
