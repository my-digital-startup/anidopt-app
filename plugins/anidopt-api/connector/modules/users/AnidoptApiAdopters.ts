import { Context } from '@nuxt/types'
import { AnidoptApiModule } from '../AnidoptApiModule'

import { Adopter, AdopterFactory } from '~/models/users/Adopter'
import { Pet, PetFactory } from '~/models/pets/Pet'

/**
 * API Module that handles Adopter models and their functions
 *
 * @function getAll()
 * @function getOneByUUID()
 * @function registerOne()
 */
export class AnidoptApiAdopters extends AnidoptApiModule {
  constructor (context: Context) {
    super('/adopters', context)
  }

  /**
   * Get all Adopters from the API
   *
   * @returns An AxiosResponse with an Adopter[] on success
   */
  public getAll (): Promise<Adopter[]> {
    return this.context.$axios.get(this.resource)
      .then((res) => {
        const data = res.data['hydra:member'] as Array<any>
        const output = [] as Adopter[]

        data.forEach((user) => {
          output.push(AdopterFactory.fromJson(user))
        })

        return output
      })
      .catch((err) => {
        throw err
      })
  }

  /**
   * Get a single Adopter from the API
   *
   * @param uuid A unique integer
   * @returns An AxiosResponse with a single Adopter instance on success
   */
  public getOneByUUID (uuid: string): Promise<Adopter> {
    return this.context.$axios.get(`${this.resource}/${uuid}`)
      .then(res => AdopterFactory.fromJson(res.data))
      .catch((err) => {
        throw err
      })
  }

  /**
   * Creates an Adopter account specifically
   *
   * [More information on the needed data](https://gitlab.com/my-digital-startup/anidopt-api-documentation)
   *
   * @param data An object that holds the specific data to create an Adopter account
   * @returns A promise that resolves a Adopter object or an error
   */
  public registerOne (data: { email: string, password: string, pseudo?: string }): Promise<Adopter> {
    return this.context.$axios.post(this.resource, data)
      .then(res => AdopterFactory.fromJson(res.data))
      .catch((err) => {
        console.log(err)
        throw err
      })
  }

  public async getFavouritePets (adopterUuid: string): Promise<Pet[]> {
    const pets = [] as Pet[]
    const res = await this.context.$axios.get(`${this.resource}/${adopterUuid}/pets`)
    const data = res.data['hydra:member'] as Array<any>

    data.map(async (pet) => {
      pet = await this.context.$api.pets.getOneByUuid(pet.uuid)
      return pets.push(PetFactory.fromJson(pet))
    })

    return pets
  }

  public toggleFavouritePet = async (adopterUuid: string, favoriteUuid: string) => {
    const body = {
      petUuid: favoriteUuid
    }

    const res = await this.context.$axios.patch(
      `${this.resource}/${adopterUuid}/pets`,
      body,
      { headers: { 'Content-Type': 'application/merge-patch+json' } }
    )

    await this.context.$auth.fetchUser()

    return res.data
  }
}
