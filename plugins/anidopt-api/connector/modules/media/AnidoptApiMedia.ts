import { Context } from '@nuxt/types'

import { AxiosRequestConfig } from 'axios'

import { AnidoptApiModule } from '../AnidoptApiModule'
import { MediaObject } from '~/models/pets/MediaObject'

const axiosConfig: AxiosRequestConfig = {
  headers: {
    'content-type': 'multipart/form-data'
  }
}

export class AnidoptApiMedia extends AnidoptApiModule {
  constructor (context: Context) {
    super('/media_objects', context)
  }

  public getAll = async (): Promise<MediaObject[]> => {
    const res = await this.context.$axios.get(this.resource)
    const data = res.data['hydra:member'] as Array<MediaObject>

    return data as MediaObject[]
  }

  public getOneByUuid = async (mediaId: string): Promise<MediaObject> => {
    const res = await this.context.$axios.get(`${this.resource}/${mediaId}`)
    const data = res.data['hydra:member'] as MediaObject

    return data as MediaObject
  }

  public addOne = async (file: File): Promise<any> => {
    const formData = new FormData()
    formData.append('file', file)

    const res = await this.context.$axios.post(this.resource, formData, axiosConfig)
    return res.data
  }

  public deleteOne = async (rawMediaId: string): Promise<any> => {
    const mediaId = rawMediaId.replace('/api/v1/media_objects/', '')

    const res = await this.context.$axios.delete(`${this.resource}/${mediaId}`)
    return res.data
  }
}
