import { Context } from '@nuxt/types'
import { AnidoptApiMedia } from './modules/media/AnidoptApiMedia'
import { AnidoptApiPets } from './modules/pets/AnidoptApiPets'
import { AnidoptApiUsers } from './modules/users/AnidoptApiUsers'

export class AnidoptApi {
  private _users: AnidoptApiUsers
  private _pets: AnidoptApiPets
  private _media: AnidoptApiMedia

  private _context: Context
  private _baseUrl: string

  constructor (context: Context) {
    this._context = context
    this._users = new AnidoptApiUsers(this._context)
    this._pets = new AnidoptApiPets(this._context)
    this._media = new AnidoptApiMedia(this._context)
    this._baseUrl = this._context.env.API_BASE_URL as string
  }

  get users (): AnidoptApiUsers {
    return this._users
  }

  get pets (): AnidoptApiPets {
    return this._pets
  }

  get media (): AnidoptApiMedia {
    return this._media
  }

  get baseUrl (): string {
    return this._baseUrl
  }
}
