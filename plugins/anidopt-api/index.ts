/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Plugin } from '@nuxt/types'

import { AnidoptApi } from '~/plugins/anidopt-api/connector/AnidoptApi'

declare module 'vue/types/vue' {
  // this.$myInjectedFunction inside Vue components
  interface Vue {
    $api: AnidoptApi
  }
}

declare module '@nuxt/types' {
  // nuxtContext.app.$myInjectedFunction inside asyncData, fetch, plugins, middleware, nuxtServerInit
  interface NuxtAppOptions {
    $api: AnidoptApi
  }
  // nuxtContext.$myInjectedFunction
  interface Context {
    $api: AnidoptApi
  }
}

declare module 'vuex/types/index' {
  // this.$myInjectedFunction inside Vuex stores
  interface Store<S> {
    $api: AnidoptApi
  }
}

const anidoptApiConnector: Plugin = (context, inject) => {
  const connector = new AnidoptApi(context)
  inject('api', connector)
}

export const apiVersion = 'v1'

export default anidoptApiConnector
