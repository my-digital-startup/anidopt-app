/**
 * An object that represents a category, used in different ways in the application :
 * - Specific breeds
 * - Specific behaviours (outdoors, indoors)
 */
export class Category {
  public readonly id: string
  public name: string

  constructor (
    id: string,
    name: string
  ) {
    this.id = id
    this.name = name
  }

  public toJSON () {
    return { ...this }
  }
}

/**
 * An abstract class that can transform a JSON object into a formatted Category object
 *
 * @method fromJson() - Generate an entity
 */
export abstract class CategoryFactory {
  /**
   * Static method to generate a Category instance from a JSON object structure
   *
   * @param data The JSON object that contains the data to parse
   * @returns A Category class instance
   */
  public static fromJson (data: any): Category {
    return new Category(
      data.id as string,
      data.name as string
    )
  }
}
