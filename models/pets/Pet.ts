import { Category, CategoryFactory } from './Category'
import { MediaObject } from './MediaObject'
import { Shelter, ShelterFactory } from '~/models/users/Shelter'

/**
 * An object that represents a pet in the Anidopt app
 */
export class Pet {
  public readonly uuid: string
  public identificationNumber: string

  public name: string
  public slug: string
  public isMale: boolean
  public isSos: boolean
  public birthday: string
  public description: string
  public isSterilised: boolean

  public shelter: Shelter
  public species: Category
  public breeds: Category[]
  public tags: Category[]

  public mainImage: MediaObject
  public images: string[]

  public createdAt: string

  constructor (
    uuid: string,
    identificationNumber: string,

    name: string,
    slug: string,
    isMale: boolean,
    isSos: boolean,
    birthday: string,
    description: string,
    isSterilised: boolean,

    shelter: Shelter,
    species: Category,
    breeds: Category[],
    tags: Category[],

    mainImage: MediaObject,
    images: string[],

    createdAt: string
  ) {
    this.uuid = uuid
    this.identificationNumber = identificationNumber

    this.name = name
    this.slug = slug
    this.isMale = isMale
    this.isSos = isSos
    this.birthday = birthday
    this.description = description
    this.isSterilised = isSterilised

    this.shelter = shelter
    this.species = species
    this.breeds = breeds
    this.tags = tags

    this.mainImage = mainImage
    this.images = images

    this.createdAt = createdAt
  }

  public toJSON () {
    return { ...this }
  }
}

/**
 * An abstract class that can transform a JSON object into a formatted Pet object
 *
 * @method fromJson() - Generate an entity
 */
export abstract class PetFactory {
  public static fromJson (data: any): any {
    const species = data.specie || data.species
    const shelter = ShelterFactory.fromJson(data.shelter)

    // if (typeof data.shelter === 'string') {
    // }

    const breeds = [] as Category[]
    const tags = [] as Category[]

    if (data.breeds) {
      data.breeds.forEach((breed: Category) => {
        breeds.push(CategoryFactory.fromJson(breed))
      })
    }

    if (data.tags) {
      data.tags.forEach((tag: Category) => {
        tags.push(CategoryFactory.fromJson(tag))
      })
    }

    return new Pet(
      data.uuid as string,
      data.identificationNumber as string,
      data.name as string,
      data.slug as string,
      data.isMale as boolean,
      data.isSos as boolean,
      data.birthday as string,
      data.description as string,
      data.isSterilised as boolean,
      shelter,
      species,
      breeds,
      tags,
      data.mainImage as MediaObject,
      data.images as string[],
      data.createdAt as string
    )
  }
}
