/**
 * An object that represents an image or a video, attached to an URI
 */
export interface MediaObject {
  readonly '@id': string
  contentUrl: string
}
