import { Pet } from '../pets/Pet'
import { Account, AccountFactory, Role } from './Account'
import { User, UserFactory } from '~/models/users/User'

/**
 * An object that represents a professional account on Anidopt.
 *
 * This is the only structure for now that can display animals on the website.
 *
 * Not to be confused with an Adopter !
 */
export class Shelter extends User {
  public slug: string
  public name: string
  public pets: Pet[]

  constructor (
    uuid: string,
    email: string,
    account: Account,
    roles: Role[],
    slug: string,
    name: string,
    pets: Pet[],
    createdAt?: string
  ) {
    super(uuid, email, account, roles, createdAt)

    this.slug = slug
    this.name = name
    this.pets = pets
  }
}

/**
 * An abstract class that can transform a JSON object into a formatted Shelter object
 */
export abstract class ShelterFactory extends UserFactory {
  /**
   * Static method to generate an Shelter instance from a JSON object structure
   *
   * @param data The JSON object that contains the data to parse
   * @returns An Shelter class instance
   */
  static fromJson (data: any): Shelter {
    return new Shelter(
      data.uuid as string,
      data.email as string,
      AccountFactory.fromJson(data.account),
      data.roles as Role[],
      data.slug as string,
      data.name as string,
      data.pets as Pet[],
      data.createdAt as string
    )
  }
}
