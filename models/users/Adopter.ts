import { Account, AccountFactory, Role } from './Account'
import { User, UserFactory } from '~/models/users/User'

/**
 * An object that represents an Adopter account on Anidopt.
 *
 * Not to be confused with an Shelter !
 */
export class Adopter extends User {
  public firstName: string
  public lastName: string
  public pseudo: string

  public favoritePets: any[]

  constructor (
    uuid: string,
    email: string,
    account: Account,
    roles: Role[],
    firstName: string,
    lastName: string,
    pseudo: string,
    favoritePets: any[],
    createdAt?: string
  ) {
    super(uuid, email, account, roles, createdAt)

    this.firstName = firstName
    this.lastName = lastName
    this.pseudo = pseudo

    this.favoritePets = favoritePets
  }
}

/**
 * An abstract class that can transform a JSON object into a formatted Adopter object
 */
export abstract class AdopterFactory extends UserFactory {
  /**
   * Static method to generate an Adopter instance from a JSON object structure
   *
   * @param data The JSON object that contains the data to parse
   * @returns A Adopter class instance
   */
  static fromJson (data: any): Adopter {
    return new Adopter(
      data.uuid as string,
      data.email as string,
      AccountFactory.fromJson(data.account),
      data.roles as Role[],
      data.firstName as string,
      data.lastName as string,
      data.pseudo as string,
      data.favoritePets as any[],
      data.createdAt as string
    )
  }
}
