import { Account, Role } from './Account'

/**
 * A class that all Users in the Anidopt App herit from
 * This class is usually never instantiated
 */
export abstract class User {
  public readonly uuid: string
  public readonly email: string

  public account: Account
  public roles: Role[]

  public readonly createdAt?: string

  constructor (
    uuid: string,
    email: string,
    account: Account,
    roles: Role[],
    createdAt?: string
  ) {
    this.uuid = uuid
    this.email = email
    this.account = account
    this.roles = roles
    this.createdAt = createdAt
  }

  public toJSON () {
    return { ...this }
  }
}

/**
 * FACTORY INTERFACE
 */
export abstract class UserFactory {
  abstract fromJson(data: any): User
}
