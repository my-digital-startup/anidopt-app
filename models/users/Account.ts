export class Account {
  public isVerified: boolean
  public address: string
  public city: string
  public zipcode: string

  constructor (
    isVerified: boolean,
    address: string,
    city: string,
    zipcode: string
  ) {
    this.isVerified = isVerified
    this.address = address
    this.city = city
    this.zipcode = zipcode
  }

  public toJSON () {
    return { ...this }
  }
}

export abstract class AccountFactory {
  /**
   * Static method to generate an Account instance from a JSON object structure
   *
   * @param data The JSON object that contains the data to parse
   * @returns An Account class instance
   */
  static fromJson (data: any): Account {
    return new Account(
      data.isVerified as boolean,
      data.address as string,
      data.city as string,
      data.zipcode as string
    )
  }
}

export enum Role {
  Normal = 'ROLE_USER',
  Adopter = 'ROLE_ADOPTER',
  Shelter = 'ROLE_SHELTER'
}
