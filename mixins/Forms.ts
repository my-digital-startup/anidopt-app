import Vue from 'vue'

export const Forms = Vue.extend({
  data () {
    return {
      form: {
        submitted: false,
        loading: false,
        finished: false
      },

      errors: {
        form: ''
      }
    }
  }
})
