/* eslint-disable vue/one-component-per-file */
import mixins from 'vue-typed-mixins'

import { InputStates } from './Inputs'

export const ButtonLike = mixins(InputStates).extend({
  props: {
    variant: {
      type: String,
      default: 'primary',
      validator: val => ['primary', 'secondary', 'green', 'yellow', 'red'].includes(val)
    },

    icon: {
      type: Array,
      default: () => [] as string[],
      required: false
    },

    inline: {
      type: Boolean,
      default: false
    },

    small: {
      type: Boolean,
      required: false,
      default: false
    }
  }
})
