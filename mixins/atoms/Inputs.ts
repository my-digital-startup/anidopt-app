/* eslint-disable vue/one-component-per-file */
import Vue from 'vue'
import mixins from 'vue-typed-mixins'

export const InputStates = Vue.extend({
  props: {
    error: {
      type: Boolean,
      default: false
    },

    warning: {
      type: Boolean,
      default: false
    },

    valid: {
      type: Boolean,
      default: false
    },

    required: {
      type: Boolean,
      default: false
    },

    disabled: {
      type: Boolean,
      default: false
    }
  }
})

export const TextInput = mixins(InputStates).extend({
  props: {
    value: {
      type: String,
      default: ''
    },

    id: {
      type: String,
      required: true
    },

    label: {
      type: String,
      default: ''
    },

    required: {
      type: Boolean,
      default: false
    },

    placeholder: {
      type: String,
      default: ''
    },

    disabled: {
      type: Boolean,
      default: false
    },

    borderless: {
      type: Boolean,
      default: false
    }
  },

  methods: {
    handleInput (e: Event) {
      const output = (e.target as HTMLInputElement).value
      this.$emit('input', output)
    }
  }
})
