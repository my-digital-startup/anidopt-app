import Vue, { PropType } from 'vue'
import { Pet } from '~/models/pets/Pet'
import { formatIsoString } from '~/utils/converters'

export const PetCard = Vue.extend({
  props: {
    pet: {
      type: Object as PropType<Pet>,
      required: true
    },

    full: {
      type: Boolean,
      required: false,
      default: false
    },

    big: {
      type: Boolean,
      required: false,
      default: false
    },

    small: {
      type: Boolean,
      required: false,
      default: false
    },

    simple: {
      type: Boolean,
      required: false,
      default: false
    },

    deployable: {
      type: Boolean,
      required: false,
      default: false
    }
  },

  data () {
    return {
      deployed: false,
      renderedPet: this.pet
    }
  },

  computed: {
    isDeployable (): Boolean {
      return Boolean(this.renderedPet.description && this.deployable)
    },

    datePosted (): string {
      return formatIsoString(this.renderedPet.createdAt)
    },

    hasCoverPhoto (): Boolean {
      return Boolean(this.renderedPet.mainImage)
    },

    mainPhotoSample (): string {
      if (this.renderedPet.species.name === 'Chien') {
        return '/sample-dog.png'
      } else if (this.renderedPet.species.name === 'Chat') {
        return '/sample-cat.png'
      } else {
        return '/sample-bird.png'
      }
    }
  },

  methods: {
    deployCard () {
      this.deployed = !this.deployed
    }
  }
})
