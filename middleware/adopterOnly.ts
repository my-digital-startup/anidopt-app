import { Middleware } from '@nuxt/types'
import { accountType } from '~/utils/types'

const adopterOnly: Middleware = ({ $auth, redirect, from, route }) => {
  if (accountType($auth.user) !== 'adopter') {
    if (route.path !== from.path) {
      return redirect(from.path)
    } else {
      return redirect('/')
    }
  }
}

export default adopterOnly
