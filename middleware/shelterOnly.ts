import { Middleware } from '@nuxt/types'
import { accountType } from '~/utils/types'

const shelterOnly: Middleware = ({ $auth, redirect, from, route }) => {
  if (accountType($auth.user) !== 'shelter') {
    if (route.path !== from.path) {
      return redirect(from.path)
    } else {
      return redirect('/')
    }
  }
}

export default shelterOnly
