/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */

import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators'
import { Category } from '~/models/pets/Category'
import { Pet } from '~/models/pets/Pet'
import { AnidoptApi } from '~/plugins/anidopt-api/connector/AnidoptApi'
@Module({
  name: 'pets',
  namespaced: true,
  stateFactory: true
})
export default class PetStore extends VuexModule {
  private _allPets = [] as Pet[]

  private _allSpecies = [] as Category[]
  private _allBreeds = [] as Category[]
  private _allTags = [] as Category[]

  get pets (): Pet[] {
    return this._allPets
  }

  get allSpecies (): Category[] {
    return this._allSpecies
  }

  get allBreeds (): Category[] {
    return this._allBreeds
  }

  get allTags (): Category[] {
    return this._allTags
  }

  @Mutation
  setPets (pets: Pet[]) {
    this._allPets = pets
  }

  @Mutation
  setSpecies (species: Category[]) {
    this._allSpecies = species
  }

  @Mutation
  setBreeds (breeds: Category[]) {
    this._allBreeds = breeds
  }

  @Mutation
  setTags (tags: Category[]) {
    this._allTags = tags
  }

  @Action({ commit: 'setPets', rawError: true })
  loadPets ($api: AnidoptApi) {
    return $api.pets.getAll()
  }

  @Action({ commit: 'setSpecies', rawError: true })
  loadSpecies ($api: AnidoptApi) {
    return $api.pets.species.getAll()
  }

  @Action({ commit: 'setBreeds', rawError: true })
  loadBreeds ($api: AnidoptApi) {
    return $api.pets.breeds.getAll()
  }

  @Action({ commit: 'setTags', rawError: true })
  loadTags ($api: AnidoptApi) {
    return $api.pets.tags.getAll()
  }
}
