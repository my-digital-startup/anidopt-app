import { Context } from '@nuxt/types'
import { Store } from 'vuex'
import { initialiseStores } from '~/utils/store-accessor'

export const actions = {
  async nuxtServerInit (store: Store<any>, context: Context) {
    await store.dispatch('pets/loadPets', context.$api)
    await store.dispatch('pets/loadBreeds', context.$api)
    await store.dispatch('pets/loadSpecies', context.$api)
  }
}

export const plugins = [initialiseStores]
