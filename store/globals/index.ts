/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */

import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators'

@Module({
  name: 'globals',
  namespaced: true,
  stateFactory: true
})
export default class GlobalStore extends VuexModule {
  private _drawerOpen: boolean = false

  get drawerOpen (): boolean {
    return this._drawerOpen
  }

  @Mutation
  setDrawerState (state: boolean) {
    this._drawerOpen = state
  }

  @Action({ commit: 'setDrawerState', rawError: true })
  toggleDrawer () {
    return !this.drawerOpen
  }
}
